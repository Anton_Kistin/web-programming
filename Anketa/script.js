﻿function check()
{
	surname = document.getElementById('surname').value;
	name = document.getElementById('name').value;
	patronymic = document.getElementById('patronymic').value;
	number = document.getElementById('number').value;
	month = document.getElementById('month').value;
	year = document.getElementById('year').value;
	mail = document.getElementById('mail').value;
	phone = document.getElementById('phone').value;
	
	var check_surname = /^[а-яА-Яa-zA-Z]+$/;
	
	if(check_surname.test(surname))
	{
		document.getElementById('picture1').innerHTML = '<img src="gal.png"></img>';
	}
	else
	{
		document.getElementById('picture2').innerHTML = '<img src="red.png"></img>';
	}
	
	var check_name = /^[а-яА-Яa-zA-Z]+$/;
	
	if(check_name.test(name))
	{
		document.getElementById('picture3').innerHTML = '<img src="gal.png"></img>';
	}
	else
	{
		document.getElementById('picture4').innerHTML = '<img src="red.png"></img>';
	}
	
	var check_patronymic = /^[а-яА-Яa-zA-Z]+$/;
	
	if(check_patronymic.test(patronymic))
	{
		document.getElementById('picture5').innerHTML = '<img src="gal.png"></img>';
	}
	else
	{
		document.getElementById('picture6').innerHTML = '<img src="red.png"></img>';
	}
	
	var check_number = /[^1-9]/;
	
	if(!check_number.test(number))
	{
		document.getElementById('picture7').innerHTML = '<img src="gal.png"></img>';
	}
	else
	{
		document.getElementById('picture8').innerHTML = '<img src="red.png"></img>';
	}
	
	var check_month = /[^1-9]/;
	
	if(!check_month.test(month))
	{
		document.getElementById('picture9').innerHTML = '<img src="gal.png"></img>';
	}
	else
	{
		document.getElementById('picture10').innerHTML = '<img src="red.png"></img>';
	}
	
	var check_year = /[^1-9]/;
	
	if(!check_year.test(year))
	{
		document.getElementById('picture11').innerHTML = '<img src="gal.png"></img>';
	}
	else
	{
		document.getElementById('picture12').innerHTML = '<img src="red.png"></img>';
	}
	
	var check_mail = /^[\w]{1}[\w-\.]*@[\w-]+\.[a-z]{2,4}$/i;
	
	if(!check_mail.test(mail))
	{
		document.getElementById('picture13').innerHTML = '<img src="red.png"></img>';
	}
	else
	{
		document.getElementById('picture14').innerHTML = '<img src="gal.png"></img>';
	}
	
	var check_phone = /^\d[\d\(\)\ -]{6,16}\d$/;
	
	if(!check_phone.test(phone))
	{
		document.getElementById('picture15').innerHTML = '<img src="red.png"></img>';
	}
	else
	{
		document.getElementById('picture16').innerHTML = '<img src="gal.png"></img>';
	}
}