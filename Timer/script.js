﻿function theTimer() 
{
	var getYear = document.getElementById('theChoiceOftheYear').value;
	var getMonth = document.getElementById('theSelectMonth').value;
	var getNumber = document.getElementById('theSelectOfNumber').value;
	var getHours = document.getElementById('theSelectHours').value;
	var getMinutes = document.getElementById('theSenterTheMinutes').value;
	
	newTime = new Date (getYear, getMonth, getNumber, getHours, getMinutes);
    netDay = new Date(); 
	var theResultOfTimer = newTime - netDay;
	
	if(theResultOfTimer > 1)
	{
		netDay = Math.floor((newTime-netDay)/1000);
		netSecond = netDay % 60; 
		netDay = Math.floor(netDay/60); 
		
		if(netSecond < 10)
		{
			netSecond = '0' + netSecond;
		}
		
		netMinute = netDay % 60; 
		netDay = Math.floor(netDay/60); 
		
		if(netMinute < 10)
		{
			netMinute = '0' + netMinute;
		}
		
		netHour = netDay % 24; 
		netDay = Math.floor(netDay/24);
		TimeOfTimer = netDay + "" + netHour + "" + netMinute + "" + netSecond + "";
	
		getStartingTheTimer = window.setTimeout("theTimer()",100);
	
		if(TimeOfTimer == 0)
		{
			document.getElementById ('theMessage').innerHTML = 'Таймер закончил отсчет!!!';
			document.getElementById ('theAnimation').innerHTML = '<img src = "animation.gif"></img>'
			clearTimeout(getStartingTheTimer);
		}
	
		document.getElementById('theDays').innerHTML =  netDay;
		document.getElementById('theHours').innerHTML = netHour;
		document.getElementById('theMinutes').innerHTML = netMinute;
		document.getElementById('theSeconds').innerHTML = netSecond;
	}
	
	else
	{
		document.getElementById('theDays').innerHTML = '00';
		document.getElementById('theHours').innerHTML = '00';
		document.getElementById('theMinutes').innerHTML = '00';
		document.getElementById('theSeconds').innerHTML = '00';
		setTimeout("theTimer()",100);
	}
}