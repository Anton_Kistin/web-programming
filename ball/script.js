function moveTheBallByClicking()
{
	var field = document.getElementById('field');
	var ball = document.getElementById('ball');
		
	field.onclick = function(event){
	
		//координаты поля относительно окна
		var fieldCoords = this.getBoundingClientRect();
		 
		//координаты левого-верхнего внутреннего угла поля
		var fieldInnerCoords = {
			top: fieldCoords.top + field.clientTop,
			left: fieldCoords.left + field.clientLeft
		    };
		
		var ballCoords = {
			top: event.clientY - fieldInnerCoords.top - ball.clientHeight/2, 
			left: event.clientX - fieldInnerCoords.left - ball.clientWidth/2
		};
		
		//не позволяем мячику выходить за верхнюю границу поля
		if(ballCoords.top < 0)
		{
			ballCoords.top = 0;
		}
		
		//не позволяем мячику выходить за левую границу поля
		if(ballCoords.left < 0) 
		{
			ballCoords.left = 0;
		}
		
		//не позволяем мячику выходить за правую границу поля
		if(ballCoords.left + ball.clientWidth > field.clientWidth)
		{
			ballCoords.left = field.clientWidth - ball.clientWidth;
		}
		
		//не позволяем мячику выходить за нижнюю границу поля
		if(ballCoords.top + ball.clientHeight > field.clientHeight)
		{
			ballCoords.top = field.clientHeight - ball.clientHeight;
		}
		ball.style.left = ballCoords.left + 'px';
		ball.style.top = ballCoords.top + 'px';
	}
}

function moveTheBallAcrossTheFieldBehindTheCursor()
{
	var field = document.getElementById('field');
	var ball = document.getElementById('ball');
		
	field.onmousemove = function(event){
	
		//координаты поля относительно окна
		var fieldCoords = this.getBoundingClientRect();
		 
		//координаты левого-верхнего внутреннего угла поля
		var fieldInnerCoords = {
			top: fieldCoords.top + field.clientTop,
			left: fieldCoords.left + field.clientLeft
		    };
		
		var ballCoords = {
			top: event.clientY - fieldInnerCoords.top - ball.clientHeight/2, 
			left: event.clientX - fieldInnerCoords.left - ball.clientWidth/2
		};
		
		//не позволяем мячику выходить за верхнюю границу поля
		if(ballCoords.top < 0)
		{
			ballCoords.top = 0;
		}
		
		//не позволяем мячику выходить за левую границу поля
		if(ballCoords.left < 0) 
		{
			ballCoords.left = 0;
		}
		
		//не позволяем мячику выходить за правую границу поля
		if(ballCoords.left + ball.clientWidth > field.clientWidth)
		{
			ballCoords.left = field.clientWidth - ball.clientWidth;
		}
		
		//не позволяем мячику выходить за нижнюю границу поля
		if(ballCoords.top + ball.clientHeight > field.clientHeight)
		{
			ballCoords.top = field.clientHeight - ball.clientHeight;
		}
		ball.style.left = ballCoords.left + 'px';
		ball.style.top = ballCoords.top + 'px';
	}
}